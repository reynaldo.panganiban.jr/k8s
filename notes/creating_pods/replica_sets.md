## Replication Controllers and ReplicaSets

# Replication Controller (Old)
```
# rc-definition.yml
apiVersion: v1
kind: ReplicationController
metadata:
  name: myapp-rc
  labels:
    app: myapp
    type: front-end
spec:
# define the pod template
  template:
    metadata:
      name: myapp-pod
      labels:
        app: myapp
        type: front-end
    spec:
      containers:
        - name: nginx-container
          image: nginx
# define how many replicas are to be maintained
  replicas: 3
```
- `kubectl create -f rc-definition.yml`
- `kubectl get replicationcontroller`
- `kubectl get pods`

# Replica set (recommended)
```
# replicaset-definition.yml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: myapp-replicaset
  labels:
    app: myapp
    type: front-end
spec:
# define the pod template
  template:
    metadata:
      name: myapp-pod
      labels:
        app: myapp
        type: front-end
    spec:
      containers:
        - name: nginx-container
          image: nginx
# define how many replicas are to be maintained
  replicas: 3
# helps replication controller what pods are under the replica set
  selector:
    matchLabels:
      type: front-end
```
- `kubectl create -f replicaset-definition.yml`
- `kubectl get replicaset`
- `kubectl get pods`
- `kubectl delete replicaset myapp-replicaset` - also deletes all underlying PODs

# Scale
- modify the number of replicas from the definition file then run `kubectl replace -f replicaset-definition.yml`
- run `kubectl scale --replicas=6 -f replicaset-definition.yml`