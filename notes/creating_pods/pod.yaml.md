```
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    app: nginx
    tier: frontend
spec:
  containers:
    - name: nginx
      image: nginx
      env:
        - name: POSTGRES_PASSWORD
          value: mysecretpassword
## creating second container
#    - name: busybox
#      image: busybox
```