## pod has 4 required top level properties
* `apiVersion:` the version of the Kubernetes API
* `kind:` refers to the type of object you are trying to create
* `metadata:` data about the object. This is in a form of dictionary
* `spec:` specification - this is where the additional information or configuration of the kubernetes we are creating

```
# pod-definition.yml
apiVersion: v1
kind: Pod
metdata:
  name: myapp-pod
  labels:
    app: myapp
    type: front-end
spec:
  containers:
    - name: nginx-container
      image: nginx
```
To create this pod, run `kubectl create -f pod-definition.yml`

commands to check the created pod
```
[rmp@local-yumrepo my_app_nginx]$ kubectl get pods
NAME        READY   STATUS    RESTARTS     AGE
myapp-pod   1/1     Running   0            53s


[rmp@local-yumrepo my_app_nginx]$ kubectl describe pod myapp-pod
Name:             myapp-pod
Namespace:        default
Priority:         0
Service Account:  default
Node:             minikube/192.168.49.2
Start Time:       Tue, 02 Jan 2024 08:52:12 +0800
Labels:           app=myapp
                  type=front-end
Annotations:      <none>
Status:           Running
IP:               10.244.0.8
IPs:
  IP:  10.244.0.8
Containers:
  nginx-container:
    Container ID:   docker://ab9b4b030e92155b1c0d6af333c640a78a6ffb765f30a20d33d59b79390185e8
    Image:          nginx
    Image ID:       docker-pullable://nginx@sha256:2bdc49f2f8ae8d8dc50ed00f2ee56d00385c6f8bc8a8b320d0a294d9e3b49026
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Tue, 02 Jan 2024 08:52:15 +0800
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-tmmxm (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-tmmxm:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  58s   default-scheduler  Successfully assigned default/myapp-pod to minikube
  Normal  Pulling    58s   kubelet            Pulling image "nginx"
  Normal  Pulled     56s   kubelet            Successfully pulled image "nginx" in 2.438s (2.438s including waiting)
  Normal  Created    56s   kubelet            Created container nginx-container
  Normal  Started    56s   kubelet            Started container nginx-container
```