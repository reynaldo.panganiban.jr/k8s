## Deployment

```
# deployment-definition.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp-deployment
  labels:
    app: myapp
    type: front-end
spec:
  template:
    metadata:
      name: myapp-pod
      labels:
        app: myapp
        type: front-end
    spec:
      containers:
        - name: nginx-container
          image: nginx
  replicas: 3
  selector:
    matchLabels:
      type: front-end
```
- `kubectl create -f deployment-definition.yaml --record && kubectl rollout status deployment/myapp-deployment` - --record will record the cause of the deployment
```
[rmp@local-yumrepo deployment]$ kubectl rollout history deployment/myapp-deployment
deployment.apps/myapp-deployment
REVISION  CHANGE-CAUSE
1         kubectl create --filename=deployment-definition.yaml --record=true
```
- `kubectl get deployments`
- `kubectl get rs`
- `kubectl get pods`
- `kubectl get all`

##  Updates and Rollback - Rollout and versioning
- `kubectl rollout status deployment/myapp-deployment`
- `kubectl rollout history deployment/myapp-deployment`

# 2 types of deployment strategy
- recreate strategy - not the recommended way because you have to destroy the deployment before creating the new ones
- rolling update - this is the default deployment strategy. seamless upgrade
    once we are done changing the version from the deployment definition file, we run `kubectl apply -f deployment-definition.yml`

To rollback the upgrade:
- `kubectl rollout undo deployment/myapp-deployment`