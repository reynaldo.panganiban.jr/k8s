```
kubectl run nginx --image nginx

kubectl get pods

minikube start
kubectl run nginx --image=nginx

# apply the configuration changes of nginx.yaml file
kubectl apply -f nginx.yaml

kubectl get pods
kubectl describe pod nginx
kubectl get pods -o wide
```

## Creating a yaml file from kubectl run command
```
[rmp@local-yumrepo my_app_nginx]$ kubectl run redis --image=redis --dry-run=client -o yaml > redis.yaml
[rmp@local-yumrepo my_app_nginx]$ cat redis.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: redis
  name: redis
spec:
  containers:
  - image: redis
    name: redis
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```